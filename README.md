# review Checklist

### Step 1 - check if the code review request is valid

- [ ] [ONLY FOR FIRST REVIEW] Verify if repo looks professional - as requested [here](https://microverse.pathwright.com/library/fast-track-curriculum/69047/path/step/66243642/)
  - How to tell if a repo is not professional:
    - There is no custom README added (similar to the one suggested in a template ☝️) **[*Caveat*] For OOP milestones 1-3 projects README is not required.**
    - Commits messages are meaningful (it is enough if they do not look like "Added something" or "next commit", see screenshots [here](https://microverse.pathwright.com/library/fast-track-curriculum/69047/path/step/66243642/))
    - Pull Requests has a meaningful title and short summary
  - Ask the student to make their repo professional and to resubmit for review. Example:
    - Please take a step back and read this lesson here https://microverse.pathwright.com/library/fast-track-curriculum/69047/path/step/66243642/.
    - after reading the lesson, Please implement the three rules as demonestrated.
    - This Code Review Request will be marked as invalid in your Dashboard, so please submit a new one once you are ready!
  - ** don't start the review if the project isn't professional. **
- [ ] Verify if the project wasn't approved already.
  - **Do not even start any review - report Code Review Request as invalid and submit a [ZenDesk ticket](https://microverse.zendesk.com/hc/en-us) to the dev team to approve it.**
- [ ] Verify correct use of the Github Flow or Git Flow [[more]](./03_flows.md) as requested [here](https://microverse.pathwright.com/library/microverse-onboarding/86777/path/step/53987922/) and [here](https://microverse.pathwright.com/library/fast-track-curriculum/69047/path/step/59882084/)
  - Ask the student to use Github/Git Flow in the CURRENT project. Example:
    - You should use Github/Git Flow in this project.
    - Please take a step back and watch this video [LINK to Pathwright lesson].
    - Then create a brand new repo with the correct setup and feature branch (with DESCRIPTIVE name) and copy your code there or close this pull request and create another one with the correct feature branch.
  - **Don't start the review**
- [ ] Verify that the project is built using the required technologies for that section of the curriculum. The technologies used by the student should not replace the main stack to be used for the project.
  - Recognize the effort the student has put into using/learning different technologies. Example:
    - It's awesome that you went out of your way to make this project with (add the technology the student used here), but please remember you should submit projects that use mainly (add the STACK that should be used here) at this point of the curriculum.
    > Please go back and redo the project using (the specified stack) then request a new code review.
    - **Don't start the review**
  - NOTE: A student can use other technologies as long as it doesn't replace the main stack to be used for the project.
    >Wrong: react/Vue/template library for HTML/CSS projects, Node for rails project.
    >Good: Some JS in an HTML project to style.
- [ ] Verify that the project is working. If the HTML website is empty or Ruby/JS app does not start or if all specs are failing, you can treat this Code Review Request as wrong/invalid.

  - Ask the student to submit a working project, if you noticed the reason for the bug - share it with the student. Example:
    - You should submit a working project. If you cannot fix it by yourself, reach out to a full-time TSE using [This link](https://microverse.zendesk.com/hc/en-us/articles/360026625973-How-can-I-get-help-from-TSEs-).  
    - This Code Review Request will be marked as invalid in your Dashboard, so please submit a new one once you are ready!

* **Don't start the review**

### Step 3 - review Pull Request

- [ ] **Avoid a never-ending re-reviews loop.** If you are making a 10th review for a given project and there are no major bugs - approve it with a note: 
   ```md
   There is still a room for improvements but you put enough effort into this project. It is approved and if you want to make it better here is what you can do:    
   - [OPTIONAL] x
   - [OPTIONAL] y
   - [OPTIONAL] z
  ```

- [ ] Check linter errors [[more]](./04_linters.md) as requested [here](https://microverse.pathwright.com/library/fast-track-curriculum/69047/path/step/54883773/)
  - Make sure that students use config files copied from [config linter repo](https://github.com/microverseinc/linters-config#linters-config)
  - **Do not approve a project if it did not pass the linter check except for a student's first project on HTML and CSS - YouTube clone, passing linter checks is not required.**
- [ ] Open ["Code review guidelines"](https://gitlab.com/microverse/guides/tse/code_review/code_review_guidelines) based on project requirements and check the requirements for the project you are reviewing.

  - [ ] **Always remember to explain WHY you request a change - it might be obvious for you but not for anybody else. If it was a requirement in the project description - add a screenshot of it.**
  - [ ] Run the pre-built tests (when available) [[more]](./06_tests.md).
  - [ ] Go through the "Minimum Requirements Checklist" [[more]](./07_minimum.md).
    - For milestone projects please go through the **General** and **Section Specific** requirements first for each milestone.
    - Feel free to copy checkboxes with the requirements that are not met.
    - Do not copy checkboxes with requirements that are met (we do not want students to easily share full checklists with other students).
    - Leave comments directly under the line of code that you want the student to modify.
  - [ ] Go through the "Stretch Requirements Checklist" [[more]](./08_stretch.md).
    - Add `[OPTIONAL]` at the beginning of your comment and mention that it is not required to mark the project as approved.
  - [ ] [OPTIONAL] Add your own comments [[more]](./09_comments.md).
    - Add `[OPTIONAL]` at the beginning of your comment and mention that it is nor required to mark the project as approved.

- [ ] Watch this [video](https://www.loom.com/share/e847c46aa4814534b23b3a8e4dd60d6d) to learn how to add GitHub inline comment to specific sections of the codebase before writing the general summary.

- [ ] Using GitHub Review features for your code review is mandatory [[more]](https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/about-pull-request-reviews).
  1. Click in 'Files changed' at the top of the Pull Request.
  2. Click 'Review changes' at the top right.
  3. Write your comments as described above in the comment box that will show up.
     ![Starting a review](../images/start_review.png)

### Step 4 - complete code review request

- [ ] Summarize the result of your code review [[more]](./10_summary.md).

  - Write the review status, `Approve` **OR** `Request changes`, at the top of your comments.
  - Select either the `Approve` **OR** `Request changes` not `Comment` option, and submit your review in the PR.
    ![Submitting a review](../images/submit_review.png)
  - [ ] [IMPORTANT] Just adding your review as a comment at the bottom of the PR and writing its status is not enough. You should follow the process described at the last item of Step 3 and add your comments at the `Review changes` comment tab.
  - Remember about giving a kind summary in the final comment.
  - **Find positive words to share in your summary, particularly when you have requested for changes multiple times. This is when they are needed the most.**.

- [ ] [OPTIONAL] Remember about emojis and gifs. They will make your code review more human-friendly 👽👽👽.
- [ ] [OPTIONAL] Open a "Merge request" in [Code review guidelines repo](https://gitlab.com/microverse/guides/tse/code_review/code_review_guidelines#how-to-raise-a-pr-for-this-repository).
  - If you think that something is missing in the requirements and/or code review guidelines.
- [ ] Mark review as completed in the [dashboard](https://dashboard.microverse.org/code_review_request) [[more]](./05_complete.md).
